import logo from './logo.png';

function component() {
  const element = document.createElement('div');
  // Add the image to our existing div.
  let img = document.createElement('img');
  img.src = logo;

  element.appendChild(img);

  return element;
}

export default component;
